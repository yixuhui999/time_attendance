// 外出申请记录表（表单号，姓名，所属部门，开始时间，结束时间，外出事由，申请状态（待定、批准、拒绝））
export class Out {
  id: number;
  name: string;
  department: string;
  bdate: string;
  edate: string;
  reason: string;
  status: string;
}

