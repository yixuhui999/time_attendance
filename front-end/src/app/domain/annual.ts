// 可休年假表（表单号，姓名，所属部门，可休年假天数）
export class Annual {
  id: number;
  name: string;
  department: string;
  days: number;
}

