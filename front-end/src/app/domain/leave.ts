// 请假申请记录表（表单号，姓名，所属部门，开始时间，结束时间，请假类别（事假、病假、婚假、产假、年假），请假事由，请假状态（待定、批准、拒绝））
export class Leave {
  id: number;
  name: string;
  department: string;
  bdate: string;
  edate:string;
  totaltime:string;
  categort: string;
}

