// 职员表（员工ID，姓名，职位，所属部门，账号，密码）
export class Staff {
  id: number;
  name: string;
  position: string;
  department: string;
  account: string;
  password: string;
}
