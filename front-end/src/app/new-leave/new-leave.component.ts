import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {Days} from '../dataclass';
declare var $: any;
@Component({
  selector: 'app-new-leave',
  templateUrl: './new-leave.component.html',
  styleUrls: ['./new-leave.component.css'],
})
export class NewLeaveComponent implements OnInit {
  Days: Days = {
    days : 0,
  };
  name = '';
  deparment = '';
  begintime = '';
  endtime = '';
  totaltime = '';
  reason = '';
  radio;
  i;
  choosevalue ;
  constructor(private location: Location) { }

  ngOnInit( ) {
    $.ajax({
      type: 'GET',
      url: 'http://localhost:3000/login',
      dataType: 'json',
      success: callback
    });
    function callback(data) {
      (document.getElementById('name')as HTMLInputElement).value = data[0].name;
      (document.getElementById('department')as HTMLInputElement).value = data[0].department;
    }
    $(document).ready(function () {
      $('#submit').click(function () {
        if (this.name ==(document.getElementById('name')as HTMLInputElement).value&&this.department ==(document.getElementById('department')as HTMLInputElement).value&&this.begintime ==(document.getElementById('begintime')as HTMLInputElement).value&&this.endtime ==(document.getElementById('endtime')as HTMLInputElement).value&&this.totaltime ==(document.getElementById('totaltime')as HTMLInputElement).value&&this.reason ==(document.getElementById('reason')as HTMLInputElement).value) {
        alert("请勿提交重复申请！")
        }
        else {
          this.name = (document.getElementById('name')as HTMLInputElement).value;
          this.department = (document.getElementById('department')as HTMLInputElement).value;
          this.begintime = (document.getElementById('begintime')as HTMLInputElement).value;
          this.endtime = (document.getElementById('endtime')as HTMLInputElement).value;
          this.totaltime = (document.getElementById('totaltime')as HTMLInputElement).value;
          this.reason = (document.getElementById('reason')as HTMLInputElement).value;
          if (this.name != "") {
            if (this.department != "") {
              var str =this.totaltime;
              var strNum: number = <number> str;
              if (strNum > 0 && this.totaltime != "NaN") {
                if (this.reason !="") {
                  $.ajax({
                    type: 'post',
                    url: 'http://localhost:3000/leaves',
                    data: {
                      "name": (document.getElementById('name')as HTMLInputElement).value,
                      "department": (document.getElementById('department')as HTMLInputElement).value,
                      "bdate": (document.getElementById('begintime')as HTMLInputElement).value,
                      "edate": (document.getElementById('endtime')as HTMLInputElement).value,
                      "totaltime": (document.getElementById('totaltime')as HTMLInputElement).value,
                      "categort": (document.getElementById('reason')as HTMLInputElement).value,
                      "state": "待审批"
                    },
                    success: function () {
                      alert('提交成功！')
                    }
                  });
                }
                else {
                  alert('请填写您的请假事由！')
                }
            }
              else {
                alert('请假日期选择有误！')
              }
          }
            else {
              alert('请填写您所在的部门！')
            }
        }
        else {
            alert('请填写您的姓名！')
          }
        }
      });
    });
  }

  showList(): void {
    const sub_menu = document.getElementById('sub_menu');
    const dis_v = sub_menu.style.display;

    if (dis_v === 'block')  {
      sub_menu.style.display = 'none';
    } else {
      sub_menu.style.display = 'block';
    }
  }

  goBack(): void {
    this.location.back();
  }
  changeTime(): void {
    var date1= (document.getElementById('begintime')as HTMLInputElement).value;  //开始时间
    var date2 = (document.getElementById('endtime')as HTMLInputElement).value;    //结束时间
    var date3 = new Date(date2).getTime() - new Date(date1).getTime();   //时间差的毫秒数
    var days=Math.floor(date3/(24*3600*1000))+1;
    this.Days.days = days;
  }
  nologin():void {
    $.ajax({

      url: "http://localhost:3000/login/" + "1",
      async: true,
      type: "delete",
      success: function () {
        alert("注销成功！")
      }
    });
  }
}
