import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChecktableComponent } from './checktable/checktable.component';
import { LoginComponent} from './login/login.component';
import { ManagerlistComponent} from './managerlist/managerlist.component';
import {RecordsComponent} from './records/records.component';
import {NewVacationComponent} from './new-vacation/new-vacation.component';
import {NewLeaveComponent} from './new-leave/new-leave.component';
import {VacationConfirmComponent} from './vacation-confirm/vacation-confirm.component';
import {LeaveConfirmComponent} from './leave-confirm/leave-confirm.component';
import {ChecktableStaffComponent} from './checktable-staff/checktable-staff.component';
import {AuthGuardService} from './service/auth-guard.service';

const routes: Routes = [
  {path: 'checktable', component: ChecktableComponent},
  {path: 'checktablestaff', component: ChecktableStaffComponent},
  {path: 'login' , component: LoginComponent},
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'checktable/managerlist', component: ManagerlistComponent},
  { path: 'records', component: RecordsComponent},
  { path: 'newVacation', component: NewVacationComponent},
  { path: 'newLeave', component: NewLeaveComponent},
  { path: 'vacationConfirm', component: VacationConfirmComponent},
  { path: 'leaveConfirm', component: LeaveConfirmComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
