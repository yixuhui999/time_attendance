import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VacationConfirmComponent } from './vacation-confirm.component';

describe('VacationConfirmComponent', () => {
  let component: VacationConfirmComponent;
  let fixture: ComponentFixture<VacationConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VacationConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VacationConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
