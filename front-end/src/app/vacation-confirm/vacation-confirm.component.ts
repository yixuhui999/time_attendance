import { Component, OnInit } from '@angular/core';
import {OUTS} from '../datanumber';
import { Location } from '@angular/common';
declare var $: any;
@Component({
  selector: 'app-vacation-confirm',
  templateUrl: './vacation-confirm.component.html',
  styleUrls: ['./vacation-confirm.component.css']
})

export class VacationConfirmComponent implements OnInit {

  constructor(private location: Location) { }
  title = '考勤系统';
  outs = OUTS;
  myposition;
  ngOnInit() {
    $.ajax({
      type: 'GET',
      url: 'http://localhost:3000/login',
      dataType: 'json',
      success: callback
    });
    function callback(data) {
      this.myposition = data[0].position;
      if (this.myposition == "员工") {
        alert("对不起，您没有该权限！")

      }
      else if (this.myposition == "管理员") {
    $.ajax({
      type: 'GET',
      url: 'http://localhost:3000/vacations',
      dataType: 'json',
      success:    function callback(data) {
        const str = JSON.stringify(data);
        this.formdata = data;
        let s = '';
        let typename = '';
        for (let i = 0; i < data.length; i++) {
          typename = "newoutconfirm" + data[i].id;
          s = '<tr><td  width="100">' + data[i].id + '</td><td   width="100">' + data[i].name + '</td><td   width="100">' + data[i].department + '</td><td  width="100">' + data[i].bdate + '</td><td  width="100">' + data[i].edate + '</td><td  width="100">' + data[i].totaltime + '</td><td  width="100">' + data[i].type + '</td><td  width="100">' + data[i].categort + '</td><td  width="100">' + data[i].state + '</td><td width="100">  <form  action="" method="get">'
            + '        <label><input  name=' + typename + ' type="radio" value="同意" />同意 </label>' +
            '        <label><input  name=' + typename + ' type="radio" value="拒绝" />拒绝 </label>  </form></td></tr>';
          $('#vacations-confirm').append(s);
        }

      }
    });


  }
}
  }

  onClick(): void {
    $.ajax({
      type: 'GET',
      url: 'http://localhost:3000/login',
      dataType: 'json',
      success: callback
    });

    function callback(data) {
      this.myposition = data[0].position;
      if(this.myposition=="员工")
      {
        alert("对不起，您没有该权限！")
      }
      else if(this.myposition=="管理员") {
        $.ajax({
          type: 'GET',
          url: 'http://localhost:3000/vacations',
          dataType: 'json',
          success:    function deleteleaves(data) {
            if (data.length > 0) {
              for (let i = 0; i < data.length; i++) {
                this.radio = (document.getElementsByName('newoutconfirm' + data[i].id));
                this.selectvalue = "";
                for (let m = 0; m < this.radio.length; m++) {
                  if (this.radio[m].checked) {
                    this.selectvalue = this.radio[m].value;
                  }
                }
                if (this.selectvalue === "同意") {
                  $.ajax({
                    url: "http://localhost:3000/vacations/" + data[i].id,
                    async: true,
                    type: "delete",
                    success: function () {
                      $.ajax({
                        type: 'post',
                        url: 'http://localhost:3000/acvacations',
                        data: {
                          "name": data[i].name,
                          "department": data[i].document,
                          "bdate": data[i].bdate,
                          "edate": data[i].edate,
                          "totaltime": data[i].totaltime,
                          "type": data[i].type,
                          "categort": data[i].categort,
                          "state": "通过"
                        },
                        success: showmessage
                      });

                      function showmessage() {
                        $.ajax({
                          type: 'GET',
                          url: 'http://localhost:3000/vacations',
                          dataType: 'json',
                          success: callback
                        });

                        function callback(data) {
                          const str = JSON.stringify(data);
                          this.formdata = data;
                          let s = '';
                          let typename = '';
                          if (data.length > 0) {
                            s += ' <tr><td width="100">请假申请单号</td><td width="100">姓名</td><td width="100">部门</td><td width="100">开始时间</td><td width="100">结束时间</td><td width="100">请假时间</td><td width="100">请假类别</td><td width="100">事由</td><td width="100">状态</td><td width="120">审核</td></tr>';
                            for (let i = 0; i < data.length; i++) {
                              typename = "newoutconfirm" + data[i].id;
                              s += '<tr><td  width="100">' + data[i].id + '</td><td   width="100">' + data[i].name + '</td><td   width="100">' + data[i].department + '</td><td  width="100">' + data[i].bdate + '</td><td  width="100">' + data[i].edate + '</td><td  width="100">' + data[i].totaltime + '</td><td  width="100">' + data[i].type + '</td><td  width="100">' + data[i].categort + '</td><td  width="100">' + data[i].state + '</td><td width="100">  <form  action="" method="get">'
                                + '        <label><input  name=' + typename + ' type="radio" value="同意" />同意 </label>' +
                                '        <label><input  name=' + typename + ' type="radio" value="拒绝" />拒绝 </label>  </form></td></tr>';

                            }
                          }
                          else if (data.length == 0) {
                            s = ' <tr><td width="100">请假申请单号</td><td width="100">姓名</td><td width="100">部门</td><td width="100">开始时间</td><td width="100">结束时间</td><td width="100">请假时间</td><td width="100">请假类别</td><td width="100">事由</td><td width="100">状态</td><td width="120">审核</td></tr>';
                          }
                          document.getElementById('vacations-confirm').innerHTML = s;
                        }
                      }
                    },
                    error: function () {
                      alert("请求失败");
                    },
                    dataType: "text"
                  });

                }
                else if (this.selectvalue === "拒绝") {
                  $.ajax({
                    url: "http://localhost:3000/vacations/" + data[i].id,
                    async: true,
                    type: "delete",
                    success: function () {
                      $.ajax({
                        type: 'post',
                        url: 'http://localhost:3000/novacations',
                        data: {
                          "name": data[i].name,
                          "department": data[i].document,
                          "bdate": data[i].bdate,
                          "edate": data[i].edate,
                          "totaltime": data[i].totaltime,
                          "type": data[i].type,
                          "categort": data[i].categort,
                          "state": "拒绝"
                        },
                        success: showmessage
                      });

                      function showmessage() {
                        $.ajax({
                          type: 'GET',
                          url: 'http://localhost:3000/vacations',
                          dataType: 'json',
                          success: callback
                        });

                        function callback(data) {
                          const str = JSON.stringify(data);
                          this.formdata = data;
                          let s = '';
                          let typename = '';
                          if (data.length > 0) {
                            s += ' <tr><td width="100">请假申请单号</td><td width="100">姓名</td><td width="100">部门</td><td width="100">开始时间</td><td width="100">结束时间</td><td width="100">请假时间</td><td width="100">请假类别</td><td width="100">事由</td><td width="100">状态</td><td width="120">审核</td></tr>';
                            for (let i = 0; i < data.length; i++) {
                              typename = "newoutconfirm" + data[i].id;
                              s += '<tr><td  width="100">' + data[i].id + '</td><td   width="100">' + data[i].name + '</td><td   width="100">' + data[i].department + '</td><td  width="100">' + data[i].bdate + '</td><td  width="100">' + data[i].edate + '</td><td  width="100">' + data[i].totaltime + '</td><td  width="100">' + data[i].type + '</td><td  width="100">' + data[i].categort + '</td><td  width="100">' + data[i].state + '</td><td width="100">  <form  action="" method="get">'
                                + '        <label><input  name=' + typename + ' type="radio" value="同意" />同意 </label>' +
                                '        <label><input  name=' + typename + ' type="radio" value="拒绝" />拒绝 </label>  </form></td></tr>';

                            }
                          }
                          else if (data.length == 0) {
                            s = ' <tr><td width="100">请假申请单号</td><td width="100">姓名</td><td width="100">部门</td><td width="100">开始时间</td><td width="100">结束时间</td><td width="100">请假时间</td><td width="100">请假类别</td><td width="100">事由</td><td width="100">状态</td><td width="120">审核</td></tr>';
                          }
                          document.getElementById('vacations-confirm').innerHTML = s;
                        }
                      }
                    },
                    error: function () {
                      alert("请求失败");
                    },
                    dataType: "text"
                  });
                }

              }
              alert("信息提交成功！");
            }
            else if (data.length == 0) {
              alert("当前没有可审批的记录！");
            }
          }
        });


      }
      }
  }

  showList(): void {
    const sub_menu = document.getElementById('sub_menu');
    const dis_v = sub_menu.style.display;

    if (dis_v === 'block')  {
      sub_menu.style.display = 'none';
    } else {
      sub_menu.style.display = 'block';
    }
  }

  goBack(): void {
    this.location.back();
  }
  nologin():void {
    $.ajax({

      url: "http://localhost:3000/login/" + "1",
      async: true,
      type: "delete",
      success: function () {
        alert("注销成功！")
      }
    });
  }
}
