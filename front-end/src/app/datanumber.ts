import {Vacation} from './dataclass';
import {Out} from './dataclass';
import {Leave} from './dataclass';

export	const	VACATIONS:	Vacation[]	=	[
  {	name: '张1' , department: '财政部' , begintime: '2018-12-13', endtime: '2018-12-14', totaltime: 2, type: '事假', reason: '有事', state: '同意', },
  {	name: '张2' , department: '行政部' , begintime: '2018-12-13', endtime: '2018-12-14', totaltime: 2, type: '事假', reason: '有事', state: '同意', },
  {	name: '张3' , department: '销售部' , begintime: '2018-12-13', endtime: '2018-12-14', totaltime: 2, type: '事假', reason: '有事', state: '同意', },
  {	name: '张4' , department: '市场部' , begintime: '2018-12-13', endtime: '2018-12-14', totaltime: 2, type: '事假', reason: '有事', state: '同意', },
  {	name: '张5' , department: '财政部' , begintime: '2018-12-13', endtime: '2018-12-14', totaltime: 2, type: '事假', reason: '有事', state: '同意', }
];
export	const	OUTS:	Out[]	=	[
  {	name: '张1' , department: '财政部' , begintime: '2018-12-13', endtime: '2018-12-14', totaltime: 2,  reason: '有事', state: '同意', },
  {	name: '张2' , department: '行政部' , begintime: '2018-12-13', endtime: '2018-12-14', totaltime: 2,  reason: '有事', state: '同意', },
  {	name: '张3' , department: '销售部' , begintime: '2018-12-13', endtime: '2018-12-14', totaltime: 2,  reason: '有事', state: '同意', },
  {	name: '张4' , department: '市场部' , begintime: '2018-12-13', endtime: '2018-12-14', totaltime: 2,  reason: '有事', state: '同意', },
  {	name: '张5' , department: '财政部' , begintime: '2018-12-13', endtime: '2018-12-14', totaltime: 2,  reason: '有事', state: '同意', }
];
export	const	LEAVES:	Leave[]	=	[

];
