import { Component, OnInit } from '@angular/core';
import {VACATIONS} from '../datanumber';
import { Location } from '@angular/common';
import {Days} from '../dataclass';
import { FormsModule } from '@angular/forms';

declare var $: any;
@Component({
  selector: 'app-new-vacation',
  templateUrl: './new-vacation.component.html',
  styleUrls: ['./new-vacation.component.css'],
})
export class NewVacationComponent implements OnInit {
  Days: Days = {
   days : 0,
  };


  name = '';
  deparment = '';
  begintime = '';
  endtime = '';
  totaltime = '';
  choose = '';
  reason = '';
  radio;
  i;
  selectvalue = '';
  choosevalue ;
  bool = false;
  onClick() {
    this.choosevalue = 0 ;
    this.radio = (document.getElementsByName('vacationchoose'));
    this.deparment = (document.getElementById('department')as HTMLInputElement).value;
    for (this.i = 0; this.i < this.radio.length; this.i++) {
      if (this.radio[this.i].checked) {
        this.selectvalue = this.radio[this.i].value;
      }
    }
    if (this.begintime === '' && this.choosevalue === 0) {
      alert('请假开始日期还没有选择');
      this.choosevalue = 1;
    } else if (this.begintime !== '' ) {
      alert(this.begintime);
    }
    if (this.selectvalue === '' && this.choosevalue === 0) {
      alert('请假类别还没有选择！');
      this.choosevalue = 1;
    } else if (this.selectvalue !== '' && this.choosevalue === 0) {
      alert(this.selectvalue);
    }
  }

  constructor( private location: Location) { }

  ngOnInit() {

    $.ajax({
      type: 'GET',
      url: 'http://localhost:3000/login',
      dataType: 'json',
      success: callback
    });
    function callback(data) {
      (document.getElementById('name')as HTMLInputElement).value = data[0].name;
      (document.getElementById('department')as HTMLInputElement).value = data[0].department;
    }
    $(document).ready(function () {
      $('#submit').click(function () {
        this.bool = false;
        this.radio = (document.getElementsByName('vacationchoose'));
        for (this.i = 0; this.i < this.radio.length; this.i++) {
          if (this.radio[this.i].checked) {
            this.selectvalue = this.radio[this.i].value;
            this.bool = true;
          }
        }
        if (this.name == (document.getElementById('name')as HTMLInputElement).value && this.department == (document.getElementById('department')as HTMLInputElement).value && this.begintime == (document.getElementById('begintime')as HTMLInputElement).value && this.endtime == (document.getElementById('endtime')as HTMLInputElement).value && this.totaltime == (document.getElementById('totaltime')as HTMLInputElement).value && this.choose == this.selectvalue && this.reason == (document.getElementById('reason')as HTMLInputElement).value) {
          alert("请勿提交重复申请！")
        }
        else {
          this.name = (document.getElementById('name')as HTMLInputElement).value;
          this.department = (document.getElementById('department')as HTMLInputElement).value;
          this.begintime = (document.getElementById('begintime')as HTMLInputElement).value;
          this.endtime = (document.getElementById('endtime')as HTMLInputElement).value;
          this.totaltime = (document.getElementById('totaltime')as HTMLInputElement).value;
          this.choose = this.selectvalue;
          this.reason = (document.getElementById('reason')as HTMLInputElement).value;
          if (this.name != "") {
            if (this.department != "") {
              var str =this.totaltime;
              var strNum: number = <number> str;
              if (strNum > 0 && this.totaltime != "NaN") {
                if (this.bool) {
                  if (this.reason !="") {
                    var date1 = (document.getElementById('begintime')as HTMLInputElement).value;  //开始时间
                    var date2 = (document.getElementById('endtime')as HTMLInputElement).value;    //结束时间
                    var date3 = new Date(date2).getTime() - new Date(date1).getTime();   //时间差的毫秒数
                    var days = Math.floor(date3 / (24 * 3600 * 1000)) + 1;
                    $.ajax({
                      type: 'post',
                      url: 'http://localhost:3000/vacations',
                      data: {
                        "name": (document.getElementById('name')as HTMLInputElement).value,
                        "department": (document.getElementById('department')as HTMLInputElement).value,
                        "bdate": (document.getElementById('begintime')as HTMLInputElement).value,
                        "edate": (document.getElementById('endtime')as HTMLInputElement).value,
                        "totaltime": days,
                        "type": this.selectvalue,
                        "categort": (document.getElementById('reason')as HTMLInputElement).value,
                        "state": "待审批"
                      },
                      success: function () {
                        alert("提交成功！");
                      },
                      error: function () {
                        alert("请求失败");
                      },
                    });
                  }
                  else {
                    alert('请填写您的请假事由！')
                  }
                }
                else {
                  alert('请选择您的请假类别！')
                }
            }
              else {
                alert('请假日期选择有误！')
              }
          }
            else {
              alert('请填写您所在的部门！')
            }
        }
          else {
            alert('请填写您的姓名！')
          }
      }
      });
    });
  }

  showList(): void {
    const sub_menu = document.getElementById('sub_menu');
    const dis_v = sub_menu.style.display;

    if (dis_v === 'block')  {
      sub_menu.style.display = 'none';
    } else {
      sub_menu.style.display = 'block';
    }
  }

  goBack(): void {
    this.location.back();
  }
  changeTime(): void {
    var date1= (document.getElementById('begintime')as HTMLInputElement).value;  //开始时间
    var date2 = (document.getElementById('endtime')as HTMLInputElement).value;    //结束时间
    var date3 = new Date(date2).getTime() - new Date(date1).getTime();   //时间差的毫秒数
    var days=Math.floor(date3/(24*3600*1000))+1;
  this.Days.days = days;
}
  nologin():void {
    $.ajax({

      url: "http://localhost:3000/login/" + "1",
      async: true,
      type: "delete",
      success: function () {
        alert("注销成功！")
      }
    });
  }
}
