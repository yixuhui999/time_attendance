import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaveConfirmComponent } from './leave-confirm.component';

describe('LeaveConfirmComponent', () => {
  let component: LeaveConfirmComponent;
  let fixture: ComponentFixture<LeaveConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeaveConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaveConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
