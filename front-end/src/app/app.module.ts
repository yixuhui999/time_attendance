import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { CoreModule } from './core/core.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DatasComponent } from './datas/datas.component';
import { DatepipeComponent } from './datepipe/datepipe.component';
import { ChecktableComponent } from './checktable/checktable.component';
import { LoginComponent } from './login/login.component';
import { ManagerlistComponent } from './managerlist/managerlist.component';
import { RecordsComponent } from './records/records.component';
import { NewVacationComponent } from './new-vacation/new-vacation.component';
import { NewLeaveComponent } from './new-leave/new-leave.component';
import { VacationConfirmComponent } from './vacation-confirm/vacation-confirm.component';
import { LeaveConfirmComponent } from './leave-confirm/leave-confirm.component';

import { ApiService } from './service/api.service';
import { ChecktableStaffComponent } from './checktable-staff/checktable-staff.component';


@NgModule({
  declarations: [
    AppComponent,
    DatasComponent,
    DatepipeComponent,
    ChecktableComponent,
    LoginComponent,
    ManagerlistComponent,
    RecordsComponent,
    NewVacationComponent,
    NewLeaveComponent,
    VacationConfirmComponent,
    LeaveConfirmComponent,
    ChecktableStaffComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    HttpModule,
    FormsModule
  ],
  providers: [
    ApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
