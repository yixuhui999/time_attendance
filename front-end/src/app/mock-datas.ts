import { Data } from './data';

export const DATAS : Data[] = [
    {
        mon : 1,
        tue : 1,
        web : 1,
        thu : 1,
        fri : 1,
        sat : 3,
        sun : 1
    },
    {
        mon : 1,
        tue : 1,
        web : 1,
        thu : 4,
        fri : 1,
        sat : 1,
        sun : 1
    },
    {
        mon : 1,
        tue : 1,
        web : 2,
        thu : 1,
        fri : 1,
        sat : 0,
        sun : 0
    },
    {
        mon : 0,
        tue : 0,
        web : 0,
        thu : 0,
        fri : 0,
        sat : 0,
        sun : 0
    }
];
