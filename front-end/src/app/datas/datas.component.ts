import { Component, OnInit } from '@angular/core';
import { Data } from '../data';
import { DATAS } from '../mock-datas';

@Component({
  selector: 'app-datas',
  templateUrl: './datas.component.html',
  styleUrls: ['./datas.component.css']
})
export class DatasComponent implements OnInit {

  datas = DATAS;

  constructor() { }

  ngOnInit() {
  }

}
