export class Vacation {
  name: string;
  department: string;
  begintime: string;
  endtime: string;
  totaltime: number;
  type: string;
  reason: string;
  state: string;
}
export class Out {
  name: string;
  department: string;
  begintime: string;
  endtime: string;
  totaltime: number;
  reason: string;
  state: string;
}
export class Leave {
  name: string;
  department: string;
  bdate: string;
  edate: string;
  totaltime: number;
  categort: string;
  state: string;
  id:string
}
export class Days {
    days:number;
}


