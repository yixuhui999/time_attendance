import { Component, OnInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-checktable-staff',
  templateUrl: './checktable-staff.component.html',
  styleUrls: ['./checktable-staff.component.css']
})
export class ChecktableStaffComponent implements OnInit {

  private saveDataAry = [];
  private nowday;
  private strNowday;
  constructor() { }

  ngOnInit(): void {
    this.selectAll();
  }

  selectAll(): void {
    $.ajax({
      type: 'GET',
      url: 'http://localhost:3000/checktable',
      dataType: 'json',
      data : {'id': 1},
      success: callback
    });
    function callback(data) {
      const str = JSON.stringify(data);
      // this.nowday = this.getNowFormatDate();
      // this.signNowday = 'sign' + this.nowday;
      // alert(str);
      this.formdata = data;
      let s = '';
      s = '<tr><td></td><td></td><td></td><td></td><td></td>' +
        '<td>1<img src="assets/image/' + data[0].sign1 + '.png" width="15px" height="15px"></td>' +
        '<td>2<img src="assets/image/' + data[0].sign2 + '.png" width="15px" height="15px"></td>' +
        '</tr><tr>' +
        '<td>3<img src="assets/image/' + data[0].sign3 + '.png" width="15px" height="15px"></td>' +
        '<td>4<img src="assets/image/' + data[0].sign4 + '.png" width="15px" height="15px"></td>' +
        '<td>5<img src="assets/image/' + data[0].sign5 + '.png" width="15px" height="15px"></td>' +
        '<td>6<img src="assets/image/' + data[0].sign6 + '.png" width="15px" height="15px"></td>' +
        '<td>7<img src="assets/image/' + data[0].sign7 + '.png" width="15px" height="15px"></td>' +
        '<td>8<img src="assets/image/' + data[0].sign8 + '.png" width="15px" height="15px"></td>' +
        '<td>9<img src="assets/image/' + data[0].sign9 + '.png" width="15px" height="15px"></td>' +
        '</tr><tr>' +
        '<td>10<img src="assets/image/' + data[0].sign10 + '.png" width="15px" height="15px"></td>' +
        '<td>11<img src="assets/image/' + data[0].sign11 + '.png" width="15px" height="15px"></td>' +
        '<td>12<img src="assets/image/' + data[0].sign12 + '.png" width="15px" height="15px"></td>' +
        '<td>13<img src="assets/image/' + data[0].sign13 + '.png" width="15px" height="15px"></td>' +
        '<td>14<img src="assets/image/' + data[0].sign14 + '.png" width="15px" height="15px"></td>' +
        '<td>15<img src="assets/image/' + data[0].sign15 + '.png" width="15px" height="15px"></td>' +
        '<td>16<img src="assets/image/' + data[0].sign16 + '.png" width="15px" height="15px"></td>' +
        '</tr><tr>' +
        '<td>17<img src="assets/image/' + data[0].sign17 + '.png" width="15px" height="15px"></td>' +
        '<td>18<img src="assets/image/' + data[0].sign18 + '.png" width="15px" height="15px"></td>' +
        '<td>19<img src="assets/image/' + data[0].sign19 + '.png" width="15px" height="15px"></td>' +
        '<td>20<img src="assets/image/' + data[0].sign20 + '.png" width="15px" height="15px"></td>' +
        '<td>21<img src="assets/image/' + data[0].sign21 + '.png" width="15px" height="15px"></td>' +
        '<td>22<img src="assets/image/' + data[0].sign22 + '.png" width="15px" height="15px"></td>' +
        '<td>23<img src="assets/image/' + data[0].sign23 + '.png" width="15px" height="15px"></td>' +
        '</tr><tr>' +
        '<td>24<img src="assets/image/' + data[0].sign24 + '.png" width="15px" height="15px"></td>' +
        '<td>25<img src="assets/image/' + data[0].sign25 + '.png" width="15px" height="15px"></td>' +
        '<td>26<img src="assets/image/' + data[0].sign26 + '.png" width="15px" height="15px"></td>' +
        '<td>27<img src="assets/image/' + data[0].sign27 + '.png" width="15px" height="15px"></td>' +
        '<td>28<img src="assets/image/' + data[0].sign28 + '.png" width="15px" height="15px"></td>' +
        '<td>29<img src="assets/image/' + data[0].sign29 + '.png" width="15px" height="15px"></td>' +
        '<td>30<img src="assets/image/' + data[0].sign30 + '.png" width="15px" height="15px"></td>' +
        '</tr><tr>' +
        '<td>31<img src="assets/image/' + data[0].sign31 + '.png" width="15px" height="15px"></td>' +
        '</tr>';
      $('#tab').append(s);
      // alert(s);
    }

  }

  onClick(): void {
    const nowday = this.getNowFormatDate();
    // alert(nowday);
    $.ajax({
      type: 'GET',
      url: 'http://localhost:3000/checktable',
      dataType: 'json',
      data : {'id': 1},
      success: callback
    });
    function callback(data) {
      let result = new Array();
      for (let i = 1; i < 32 ; i ++) {
        const signnum = 'sign' + i;
        if (i === nowday) {
          result[i] = '1';
        } else {
          result[i] = data[0][signnum];
        }
      }
      $.ajax({
        type: 'PUT',
        url: 'http://localhost:3000/checktable/1',
        dataType: 'json',
        data: {
          'sign1' : result[1],
          'sign2' : result[2],
          'sign3' : result[3],
          'sign4' : result[4],
          'sign5' : result[5],
          'sign6' : result[6],
          'sign7' : result[7],
          'sign8' : result[8],
          'sign9' : result[9],
          'sign10' : result[10],
          'sign11' : result[11],
          'sign12' : result[12],
          'sign13' : result[13],
          'sign14' : result[14],
          'sign15' : result[15],
          'sign16' : result[16],
          'sign17' : result[17],
          'sign18' : result[18],
          'sign19' : result[19],
          'sign20' : result[20],
          'sign21' : result[21],
          'sign22' : result[22],
          'sign23' : result[23],
          'sign24' : result[24],
          'sign25' : result[25],
          'sign26' : result[26],
          'sign27' : result[27],
          'sign28' : result[28],
          'sign29' : result[29],
          'sign30' : result[30],
          'sign31' : result[31],
        },
        success: present
      });
      function present () {
        $.ajax({
          type: 'GET',
          url: 'http://localhost:3000/checktable',
          dataType: 'json',
          data : {'id': 1},
          success: show
        });
        function show(data) {
          let s = '';
          s = '<tr><td></td><td></td><td></td><td></td><td></td>' +
            '<td>1<img src="assets/image/' + data[0].sign1 + '.png" width="15px" height="15px"></td>' +
            '<td>2<img src="assets/image/' + data[0].sign2 + '.png" width="15px" height="15px"></td>' +
            '</tr><tr>' +
            '<td>3<img src="assets/image/' + data[0].sign3 + '.png" width="15px" height="15px"></td>' +
            '<td>4<img src="assets/image/' + data[0].sign4 + '.png" width="15px" height="15px"></td>' +
            '<td>5<img src="assets/image/' + data[0].sign5 + '.png" width="15px" height="15px"></td>' +
            '<td>6<img src="assets/image/' + data[0].sign6 + '.png" width="15px" height="15px"></td>' +
            '<td>7<img src="assets/image/' + data[0].sign7 + '.png" width="15px" height="15px"></td>' +
            '<td>8<img src="assets/image/' + data[0].sign8 + '.png" width="15px" height="15px"></td>' +
            '<td>9<img src="assets/image/' + data[0].sign9 + '.png" width="15px" height="15px"></td>' +
            '</tr><tr>' +
            '<td>10<img src="assets/image/' + data[0].sign10 + '.png" width="15px" height="15px"></td>' +
            '<td>11<img src="assets/image/' + data[0].sign11 + '.png" width="15px" height="15px"></td>' +
            '<td>12<img src="assets/image/' + data[0].sign12 + '.png" width="15px" height="15px"></td>' +
            '<td>13<img src="assets/image/' + data[0].sign13 + '.png" width="15px" height="15px"></td>' +
            '<td>14<img src="assets/image/' + data[0].sign14 + '.png" width="15px" height="15px"></td>' +
            '<td>15<img src="assets/image/' + data[0].sign15 + '.png" width="15px" height="15px"></td>' +
            '<td>16<img src="assets/image/' + data[0].sign16 + '.png" width="15px" height="15px"></td>' +
            '</tr><tr>' +
            '<td>17<img src="assets/image/' + data[0].sign17 + '.png" width="15px" height="15px"></td>' +
            '<td>18<img src="assets/image/' + data[0].sign18 + '.png" width="15px" height="15px"></td>' +
            '<td>19<img src="assets/image/' + data[0].sign19 + '.png" width="15px" height="15px"></td>' +
            '<td>20<img src="assets/image/' + data[0].sign20 + '.png" width="15px" height="15px"></td>' +
            '<td>21<img src="assets/image/' + data[0].sign21 + '.png" width="15px" height="15px"></td>' +
            '<td>22<img src="assets/image/' + data[0].sign22 + '.png" width="15px" height="15px"></td>' +
            '<td>23<img src="assets/image/' + data[0].sign23 + '.png" width="15px" height="15px"></td>' +
            '</tr><tr>' +
            '<td>24<img src="assets/image/' + data[0].sign24 + '.png" width="15px" height="15px"></td>' +
            '<td>25<img src="assets/image/' + data[0].sign25 + '.png" width="15px" height="15px"></td>' +
            '<td>26<img src="assets/image/' + data[0].sign26 + '.png" width="15px" height="15px"></td>' +
            '<td>27<img src="assets/image/' + data[0].sign27 + '.png" width="15px" height="15px"></td>' +
            '<td>28<img src="assets/image/' + data[0].sign28 + '.png" width="15px" height="15px"></td>' +
            '<td>29<img src="assets/image/' + data[0].sign29 + '.png" width="15px" height="15px"></td>' +
            '<td>30<img src="assets/image/' + data[0].sign30 + '.png" width="15px" height="15px"></td>' +
            '</tr><tr>' +
            '<td>31<img src="assets/image/' + data[0].sign31 + '.png" width="15px" height="15px"></td>' +
            '</tr>';
          document.getElementById('tab').innerHTML = s;
        }
      }

    }

  }


  showList(): void {
    const sub_menu = document.getElementById('sub_menu');
    const dis_v = sub_menu.style.display;

    if (dis_v === 'block')  {
      sub_menu.style.display = 'none';
    } else {
      sub_menu.style.display = 'block';
    }
  }

  getNowFormatDate(): number {
    const date = new Date();
    const seperator1 = '-';
    const year = date.getFullYear();
    const month: number = date.getMonth() + 1;
    let strmonth;
    let strDate = date.getDate();
    if (month > 0 && month < 10) {
      strmonth = '0' + month;
    }
    if (strDate > 0 && strDate < 10) {
      strDate = Number('0' + strDate);
    }
    const currentdate = year + seperator1 + month + seperator1 + strDate;
    return strDate;
  }
  nologin():void {
    $.ajax({

      url: "http://localhost:3000/login/" + "1",
      async: true,
      type: "delete",
      success: function () {
        alert("注销成功！")
      }
    });
  }
}
