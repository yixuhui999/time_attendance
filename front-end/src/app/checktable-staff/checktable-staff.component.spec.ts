import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChecktableStaffComponent } from './checktable-staff.component';

describe('ChecktableStaffComponent', () => {
  let component: ChecktableStaffComponent;
  let fixture: ComponentFixture<ChecktableStaffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChecktableStaffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChecktableStaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
