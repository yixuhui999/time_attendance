import { Injectable } from '@angular/core';

import { Http, Headers, Response } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import { Leave } from '../domain/leave';
import { ApiService } from './api.service';

@Injectable()
export class NewLeaveService {

  private api_url : string ;
  private headers : Headers ;

  constructor(private http: Http, private apiService: ApiService) {
    this.api_url = apiService.getUrl() + '/leaves';
    this.headers = apiService.getHeaders();
  }
  //创建一个User
  createLeave(Leave: Leave): Promise<Leave> {
    const url = `${this.api_url}`;
    return this.http
      .post(url, JSON.stringify(Leave), { headers: this.headers })
      .toPromise()
      .then(res => res.json() as Leave)
      .catch(this.handleError);
  }


  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
