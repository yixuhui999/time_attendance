import { TestBed } from '@angular/core/testing';

import { NewLeaveService} from './new-leave.service'

describe('NewLeaveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NewLeaveService = TestBed.get(NewLeaveService);
    expect(service).toBeTruthy();
  });
});
