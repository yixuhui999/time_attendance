"""Web后端 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from rest_framework import  routers
from django.urls import path
from django.conf.urls import  url ,include
from django.contrib import  admin
from firstapp import  views
from firstapp.views import staffViewSet,clockViewSet,leaveAplViewSet,outAplViewSe,annualViewSet
from rest_framework.urlpatterns import  format_suffix_patterns

router=routers.DefaultRouter()
router.register(r'staffs',staffViewSet,base_name="staffs")
router.register(r'clock',clockViewSet,base_name="clock")
router.register(r'out',leaveAplViewSet,base_name='out')
router.register(r'leave',outAplViewSe,base_name='leave')
router.register(r'annual',annualViewSet,base_name='annual')
urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^',include(router.urls)),
    url(r'^Web/',include("rest_framework.urls",namespace="rest_framework")),


]

