from rest_framework import  serializers
from firstapp.models import staff,clock,leaveApl,outApl,annual


class staffSerializer(serializers.HyperlinkedModelSerializer):
        class Meta:
            model = staff
            fields =("id","name","position","department","account","password")



class  clockSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = clock
        fields=("id","name","department","cdate")



class  leaveAplSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = leaveApl
        fields = ("id","ownerId","name","department","bdate","edate","category","reason","status")




class   outAplSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model =outApl
        fields= ("id","ownerId","name","department","bdate","edate","reason","status")




class    annualSerializer(serializers.HyperlinkedModelSerializer):
    class Meta :
        model = annual
        fields =("name","department","days")





