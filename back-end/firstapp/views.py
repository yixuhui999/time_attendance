from firstapp.models import staff,clock,leaveApl,outApl,annual
from django.shortcuts import render
from rest_framework.decorators import api_view
from django.http import HttpResponse
from rest_framework.response import Response
from firstapp.serializers import staffSerializer,clockSerializer,annualSerializer,leaveAplSerializer,outAplSerializer
from rest_framework import  viewsets , parsers, mixins
from rest_framework import  status
from django.http import JsonResponse, QueryDict

class  staffViewSet(mixins.ListModelMixin,mixins.CreateModelMixin,mixins.RetrieveModelMixin,mixins.UpdateModelMixin,mixins.DestroyModelMixin,viewsets.GenericViewSet):
      queryset =  staff.objects.all()
      serializer_class = staffSerializer


class   clockViewSet(mixins.ListModelMixin,mixins.CreateModelMixin,mixins.RetrieveModelMixin,mixins.UpdateModelMixin,mixins.DestroyModelMixin,viewsets.GenericViewSet):
       queryset = clock.objects.all()
       serializer_class = clockSerializer


class   leaveAplViewSet(mixins.ListModelMixin,mixins.CreateModelMixin,mixins.RetrieveModelMixin,mixins.UpdateModelMixin,mixins.DestroyModelMixin,viewsets.GenericViewSet):
    queryset = leaveApl.objects.all()
    serializer_class = leaveAplSerializer


class    outAplViewSe(mixins.ListModelMixin,mixins.CreateModelMixin,mixins.RetrieveModelMixin,mixins.UpdateModelMixin,mixins.DestroyModelMixin,viewsets.GenericViewSet):
    queryset = outApl.objects.all()
    serializer_class = outAplSerializer


class  annualViewSet(mixins.ListModelMixin,mixins.CreateModelMixin,mixins.RetrieveModelMixin,mixins.UpdateModelMixin,mixins.DestroyModelMixin,viewsets.GenericViewSet):
    queryset =annual.objects.all()
    serializer_class = annualSerializer


















