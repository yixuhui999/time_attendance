from django.db import models

# Create your models here.
class  staff(models.Model):
    id = models.CharField(max_length=10,primary_key=True)
    name =models.CharField(max_length=20)
    position = models.CharField(max_length=20)
    department = models.CharField(max_length=20)
    account=models.CharField(max_length=20)
    password = models.CharField(max_length=20)
class clock(models.Model):
    id =models.CharField(max_length=10,primary_key=True)
    name = models.CharField(max_length=20)
    department= models.CharField(max_length=20)
    cdate= models.DateField()
class leaveApl(models.Model):
    id = models.CharField(max_length=20,primary_key=True)
    name =models.CharField(max_length=20)
    ownerId=models.CharField(max_length=20)
    department=models.CharField(max_length=20)
    bdate=models.DateField()
    edate=models.DateField()
    category=models.CharField(max_length=20)
    reason=models.CharField(max_length=50)
    status= models.CharField(max_length=2)
class outApl(models.Model):
        id=models.CharField(max_length=20,primary_key=True)
        name = models.CharField(max_length=20)
        ownerId=models.CharField(max_length=20)
        department = models.CharField(max_length=20)
        bdate = models.DateField()
        edate = models.DateField()
        reason = models.CharField(max_length=50)
        status = models.CharField(max_length=2)
class annual(models.Model):
    name =models.CharField(max_length=20,primary_key=True)
    department=models.CharField(max_length=20)
    days=models.IntegerField()



