from django.contrib import admin
# Register your models here.
from .models import staff,clock,leaveApl,annual,outApl

class staffAdmin(admin.ModelAdmin):
 list_display = ["id","name","position","department","account","password"]  #显示字段
 list_filter = []   #筛选
 search_fields = []   #查找栏
 list_per_page = 5  # 分页

 fields = []  #修改页的顺序


admin.site.register(staff,staffAdmin)
admin.site.register(clock)
admin.site.register(leaveApl)
admin.site.register(annual)
admin.site.register(outApl)
